* Give me cookies!

[[./screen.png]]

** Implementations

*** Guile-sld2

Guile-sdl2 is a scheme binding for library SDL2.

#+BEGIN_SRC shell
$ cd guile-sdl2
$ guile -s cookies.scm
#+END_SRC


*** Chickadee

Chickadee is a game development toolkit for Guile Scheme.

#+BEGIN_SRC shell
$ cd chickadee
$ guile -s cookies.scm
#+END_SRC


*** GreenShadow

GreenShadow is a Guile Scheme game library
(https://framagit.org/prouby/greenshadow).

#+BEGIN_SRC shell
$ cd greenshadow
$ guile -s cookies.scm
#+END_SRC


*** LÖVE

LÖVE is a game engine for lua language.

#+BEGIN_SRC shell
$ cd love/cookies/
$ love .
#+END_SRC


*** Pygame

Pygame is a game module for python language.

#+BEGIN_SRC shell
$ cd pygame
$ python3 cookies.py
#+END_SRC


*** C+SDL2

C is the SDL2 native programming language.

#+BEGIN_SRC shell
$ cd c
$ gcc -Wall -o cookies cookies.c -lSDL2 -lSDL2_image -lSDL2_ttf
$ ./cookies
#+END_SRC

*** JAVA + JAVAFX

Java 8, 9 and 10 (javafx included):

#+BEGIN_SRC shell
$ javac Cookies.java
$ java Cookies
#+END_SRC

Java 11+ (need to install javafx):

#+BEGIN_SRC shell
$ javac --module-path "javafx/path/to/lib" --add-modules javafx.controls Cookies.java
$ java --module-path "javafx/path/to/lib" --add-modules javafx.controls Cookies
#+END_SRC
