;; Copyright © 2019 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(use-modules (ice-9 match)
             (chickadee)
             (chickadee math vector)
             (chickadee render)
             (chickadee render font)
             (chickadee render color)
             (chickadee render sprite)
             (chickadee render texture)
             (chickadee render viewport))

(define cookies 0)
(define sprite #f)
(define background #f)
(define list-pos-+1 '())
(define cookies-scale-vector
  (let ((scale (/ 720 2560)))
    (vec2 scale scale)))

(define (draw+1 l)
  (match l
    ((x y)
     (if (< y 720)
         (let ((ny (+ 4 y)))
           (draw-text "+1" (vec2 x ny))
           (list x ny))
         #f))
    (_ #f)))

(define (more-cookies button x y)
  (when (equal? button 'left)
    (set! cookies (+ cookies 1))
    (set! list-pos-+1
      (cons (list (- (+ x (random 50)) 25) y)
            list-pos-+1))))

;;; Load
(define (load)
  (set! background
    (make-viewport 0 0 720 720
                   #:clear-color
                   (make-color (/ 128 255)
                               (/ 200 255)
                               1 1)))
  (set! sprite (load-image "cookies.png")))

;;; Draw
(define (draw alpha)
  (with-viewport background
    (draw-sprite sprite #v(0.0 0.0)
                 #:scale cookies-scale-vector)
    (draw-text (format "Score: ~a" cookies)
               #v(260.0 700.0))
    (set! list-pos-+1
      (delete #f (map draw+1 list-pos-+1)))))

(run-game #:window-title "Give me cookies - Chickadee"
          #:window-width 720 #:window-height 720
          #:load load #:draw draw
          #:mouse-release more-cookies)
