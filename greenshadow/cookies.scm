#!@GUILE@ --no-auto-compile
-*- scheme -*-
!#
;; Copyright © 2020 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>

(use-modules (greenshadow)
             (srfi srfi-1)
             (ice-9 match))

(define add1 '())
(define cnt 0)

(define (load)
  (load-sprite 'cookie "cookies.png")
  (text->sprite 'add1 "+1"))

(define (update)
  (let ((add (map (lambda (p)
                     (match p
                       ((x y)
                        (if (< y -64) #f
                            (list x (- y 4)))))) add1)))
    (set! add1 (remove not add))))

(define (draw)
  (draw-sprite 'cookie)
  (map (lambda (p) (draw-sprite 'add1 #:pos p)) add1)
  (draw-text (number->string cnt) #:pos '(32 32)))

(define (mouse pressed? button x y)
  (when (and pressed? (equal? button 'left))
    (set! cnt (+ cnt 1))
    (set! add1 (cons (list (- (+ x (random 30)) 15) y) add1))))

(run #:size '(720 720)
     #:title "Give me cookies - GreenShadow"
     #:font "GrapeSoda.ttf"
     #:font-size 64
     #:load load
     #:update update
     #:draw draw
     #:mouse-event mouse
     #:background-color '(128 200 255 255))
