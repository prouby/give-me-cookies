/* Copyright � 2019 by ROMAINPC <romainpc.lechat@laposte.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Cookies extends Application {

	private static int score = 0;

	public void start(Stage primaryStage) {
		try {

			Group root = new Group();
			Scene scene = new Scene(root, 720, 720);
			scene.setFill(Color.rgb(128, 200, 255));

			ImageView cookieV = new ImageView(new Image("cookies.png", 720, 730, true, false));

			Label scoreL = new Label("Score : 0");

			scoreL.setFont(Font.loadFont(getClass().getResourceAsStream("GrapeSoda.ttf"), 64));
			scoreL.setTextFill(Color.WHITE);
			scoreL.setLayoutX(180);

			cookieV.setOnMouseClicked(e -> {

				Label plusOne = new Label("+1");
				plusOne.setMouseTransparent(true);
				plusOne.setLayoutX(e.getSceneX());
				plusOne.setFont(Font.loadFont(getClass().getResourceAsStream("GrapeSoda.ttf"), 64));
				plusOne.setTextFill(Color.WHITE);
				root.getChildren().add(plusOne);
				
				FadeTransition fT = new FadeTransition(Duration.seconds(2), plusOne);
				fT.setFromValue(1);fT.setToValue(0);
				
				TranslateTransition tT = new TranslateTransition(Duration.seconds(2), plusOne);
				tT.setFromY(e.getSceneY());tT.setToY(e.getSceneY() - 200);
				
				ParallelTransition pT = new ParallelTransition(fT, tT);
				pT.setOnFinished(evt ->{ root.getChildren().remove(plusOne); });
				pT.play();

				score++;
				scoreL.setText("Score : " + String.valueOf(score));
			});

			root.getChildren().addAll(cookieV, scoreL);

			primaryStage.setTitle("Give me cookies - JavaFX");
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
